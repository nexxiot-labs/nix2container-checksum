{
  description = "nix-toolbox";

  inputs = {
    std = {
      url = "github:divnix/std";
      # url = "git+file:/home/peter/thirdparty/std";

      inputs.nixago.follows = "nixago";
      inputs.devshell.follows = "devshell";
      inputs.n2c.follows = "n2c";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    devshell = {
      url = "github:numtide/devshell";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nixpkgs.url = "nixpkgs";

    yants.follows = "std/yants";
    haumea.follows = "std/haumea";
    incl.follows = "std/incl";
    n2c = {
      url = "github:nlewo/nix2container";
      # url = "github:kolloch/nix2container/fix/94-closure";
      # url = "/home/peter/thirdparty/nix2container";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    devenv.url = "github:cachix/devenv";
    devenv.inputs.nixpkgs.follows = "nixpkgs";

    namaka.url = "github:nix-community/namaka/v0.2.0";
    namaka.inputs.haumea.follows = "haumea";
    namaka.inputs.nixpkgs.follows = "nixpkgs";

    nixago.url = "github:nix-community/nixago";
    nixago.inputs.nixpkgs.follows = "nixpkgs";
    nixago.inputs.nixago-exts.follows = "";
  };

  # nixConfig = {
  #   extra-trusted-public-keys = "nx-gitlab-runner-nix-cache-cd-prd-nexiot:GqKMoaOiMSrOti2g1HxOT1aTCSFKhWjC0ibBVCd2vGU=";
  #   extra-substituters = "s3://nx-gitlab-runner-nix-cache-cd-prd-nexiot.eu-central-1?region=eu-central-1&trusted=true&want-mass-query=true&priority=45&parallel-compression=true&compression=zstd&compression-level=3";
  # };

  outputs = {
    std,
    self,
    ...
  } @ inputs:
    std.growOn {
      inputs =
        inputs
        // {
          origInputs = inputs;
        };
      cellsFrom = ./nix;
      cellBlocks = with std.blockTypes; [
        (functions "lib")
        (functions "types")
        (anything "containers")
        (installables "packages" {ci.build = true;})
        (installables "runnables" {ci.build = true;})
        (devshells "shells" {ci.build = true;})
      ];
    }
    {
      devShells = std.harvest self ["automation" "shells"];
      packages = std.harvest self [
        ["automation" "packages"]
      ];
      containers = std.harvest self [
        ["gitlab" "containers"]
        ["gitlab" "runnables"]
      ];
    };
}

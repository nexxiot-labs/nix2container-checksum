{
  cell,
  inputs,
}: let
  inherit (inputs) nixpkgs n2c;
  nix2container = n2c.packages.nix2container;
  lib = nixpkgs.lib;
in {
  # Nest all layers so that prior layers are dependencies of later layers.
  # This way, we should avoid redundant dependencies.
  foldImageLayers = let
    mergeToLayer = priorLayers: component:
      assert builtins.isList priorLayers;
      assert builtins.isAttrs component; let
        layer = nix2container.buildLayer (component
          // {
            layers = priorLayers;
          });
      in
        priorLayers ++ [layer];
  in
    layers: lib.foldl mergeToLayer [] layers;
}

{
  inputs,
  cell,
}: let
  inherit (inputs) nixpkgs n2c;
  inherit (inputs.cells.automation.lib) foldImageLayers;
  l = nixpkgs.lib // builtins;
  nix2container = n2c.packages.nix2container;

  user = "user";
  group = "user";
  uid = "1000";
  gid = "1000";

  mkDirs = nixpkgs.runCommand "mkDirs" {} ''
    mkdir -p $out/tmp
    mkdir -p $out/home/${user}
  '';

  mkUser = nixpkgs.runCommand "mkUser" {} ''
    mkdir -p $out/etc/pam.d

    echo "root:x:0:0::" > $out/etc/passwd
    echo "${user}:x:${uid}:${gid}::" >> $out/etc/passwd
    echo "root:!x:::::::" > $out/etc/shadow
    echo "${user}:!x:::::::" >> $out/etc/shadow

    echo "root:x:0:" > $out/etc/group
    echo "${group}:x:${gid}:" >> $out/etc/group
    echo "root:x::" > $out/etc/gshadow
    echo "${group}:x::" >> $out/etc/gshadow

    cat > $out/etc/pam.d/other <<EOF
    account sufficient pam_unix.so
    auth sufficient pam_rootok.so
    password requisite pam_unix.so nullok sha512
    session required pam_unix.so
    EOF

    touch $out/etc/login.defs
  '';
  nixConfig = nixpkgs.runCommand "nixConfig" {} ''
    mkdir -p $out/etc/nix
    # https://nixos.org/manual/nix/stable/command-ref/conf-file
    cat > $out/etc/nix/nix.conf <<EOF
    sandbox = true
    sandbox-fallback = false
    # fsync-metadata = true
    connect-timeout = 10
    max-silent-time = 120
    keep-outputs = true
    keep-derivations = true
    keep-failed = true
    keep-going = true
    log-lines = 100
    max-jobs = auto
    build-users-group =
    experimental-features = nix-command flakes repl-flake
    EOF

    echo "#################################################"
    echo $out
  '';

  entrypoint =
    nixpkgs.writeShellApplication
    {
      name = "entrypoint";
      text = ''
        # (nix doctor && ls -la /nix) >out 2>&1 && cat out

        # Without arguements, run bash
        if [ $# -eq 0 ]; then
          exec ${nixpkgs.bash}/bin/bash
        fi

        exec "$@"
      '';
    };
  make-nix-ci = {
    useRoot ? true,
    overrideImageSpec ? (s: s),
    reproducible ? true,
  }: let
    baseLayers = [
      {
        deps = with nixpkgs; [
          coreutils
          cacert
          bash
          gnugrep
          gitMinimal
          nix
        ];
      }
      # Dependencies of building container inside.
      # This speeds up testing because these don't need to be rebuild/fetch within the container.
      {
        deps = with n2c.packages;
        with nixpkgs; [
          nix2container-bin
          skopeo-nix2container
          diffutils
          gawk
          gcc
          gnutar
          gnumake
          jq.bin
        ];
      }
      # Problematic!!
      # {
      #   deps = with nixpkgs; [
      #     stdenv
      #   ];
      # }
      {
        deps = [entrypoint];
        copyToRoot = [
          mkDirs
          mkUser
          nixConfig
        ];

        perms =
          l.optional (!useRoot)
          {
            path = mkDirs;
            regex = "/home/${user}|/tmp";
            mode = "0744";
            uid = l.toInt uid;
            gid = l.toInt gid;
            uname = user;
            gname = group;
          };
      }
      {
        copyToRoot = [
          (nixpkgs.buildEnv {
            name = "root-links";
            paths = l.concatMap (l: l.deps or []) baseLayers;
          })
        ];
      }
    ];
    imageSpec = overrideImageSpec {
      name = "registry.gitlab.com/nexiot-ag/devops/tools/nix-toolbox/nix-ci${l.optionalString useRoot "-root"}";

      initializeNixDatabase = true;
      nixUid =
        if useRoot
        then 0
        else l.toInt uid;
      nixGid =
        if useRoot
        then 0
        else l.toInt gid;

      layers = foldImageLayers (builtins.map (l: l // {inherit reproducible;}) baseLayers);

      config = {
        Entrypoint = ["/bin/entrypoint"];
        User =
          if useRoot
          then "root"
          else "user";
        WorkingDir =
          if useRoot
          then "/root"
          else "/home/user";
        Volumes = {"/nix" = {};};
        Env =
          [
            "PATH=/bin"
            "NIX_PAGER=cat"
            "SSL_CERT_FILE=/etc/ssl/certs/ca-bundle.crt"
          ]
          ++ (l.optionals useRoot [
            "HOME=/root"
            "USER=root"
          ])
          ++ (l.optionals (!useRoot) [
            "HOME=/home/user"
            "USER=user"
          ]);
        # TODO: Labels
      };
    };
  in
    nix2container.buildImage imageSpec;
in {
  nix-ci = make-nix-ci {useRoot = false;};
  nix-ci-reproducible-false = make-nix-ci {
    useRoot = false;
    reproducible = false;
  };
  nix-ci-with-hello = make-nix-ci {
    useRoot = false;
    overrideImageSpec = s:
      s
      // {
        config =
          s.config
          // {
            copyToRoot = [nixpkgs.hello];
          };
      };
  };

  only-stdenv = nix2container.buildImage {
    name = "only-stdenv";
    copyToRoot = [nixpkgs.stdenv];
  };

  layered = nix2container.buildImage {
    name = "layered";
    layers = 
      let layerDefs = [
        {
          deps = with nixpkgs; [ readline ];
        }
        {
          deps = with nixpkgs; [ bashInteractive ];
        }
        {
          deps = with nixpkgs; [ zsh ];
        }
      ];
      in builtins.map nix2container.buildLayer layerDefs;
    config = {
      Env = [
        (let path = with nixpkgs; lib.makeBinPath [ bashInteractive zsh ];
        in "PATH=${path}")
      ];
    };
  };

  layeredDeduplicated = nix2container.buildImage {
    name = "layered";
    layers = 
      let 
        commonLayer = {
          deps = with nixpkgs; [ readline ];
        };
      layerDefs = [
        {
          deps = with nixpkgs; [ bashInteractive ];
          layers = [ (nix2container.buildLayer commonLayer) ];
        }
        {
          deps = with nixpkgs; [ zsh ];
          layers = [ (nix2container.buildLayer commonLayer) ];
        }
      ];
      in builtins.map nix2container.buildLayer layerDefs;
    config = {
      Env = [
        (let path = with nixpkgs; lib.makeBinPath [ bashInteractive zsh ];
        in "PATH=${path}")
      ];
    };
  };

layeredDeduplicatedAutomatic = nix2container.buildImage {
  name = "layeredAutomatic";
  layers = 
    let layerDefs = [
      { deps = with nixpkgs; [ readline ]; }
      { deps = with nixpkgs; [ bashInteractive ]; }
      { deps = with nixpkgs; [ zsh ]; }
    ];
    in foldImageLayers layerDefs;
  config = {
    Env = [
      (let path = with nixpkgs; lib.makeBinPath [ bashInteractive zsh ];
      in "PATH=${path}")
    ];
  };
};

  nix-ci-with-stdenv = make-nix-ci {
    useRoot = false;
    overrideImageSpec = s:
      s
      // {
        config =
          s.config
          // {
            copyToRoot = [nixpkgs.stdenv];
          };
      };
  };
  nix-ci-root = make-nix-ci {useRoot = true;};
}

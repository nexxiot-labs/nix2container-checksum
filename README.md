# nix2container-checksum

Reproduce a checksum problem.

The images for testing:

* `nix-ci`: A CI image containing nix running as user.

* `nix-ci-reproducible-false`: Like `nix-ci` but set `reproducible = false` on all layers.

* `nix-ci-root`: A CI image containing nix running as root.

* `nix-ci-with-hello`: Exactly like `nix-ci`
  just that we added hello via the `copyToRoot` arguement of `buildImage`.

The system that I tested with was an Ubuntu linux install with multi-user nix (nix running as root).

I created a couple of debug commands that collect debug information such as all store paths in the closure with checksums and the actual store paths with contents.

## Bad case: Building stdenv-only in nix-ci

```bash
nix run .\#x86_64-linux.gitlab.runnables.debug-building-stdenv-only-in-nix-ci
```

## Good case: Building nix-ci in nix-ci

```bash
nix run .\#x86_64-linux.gitlab.runnables.debug-building-nix-ci-in-nix-ci
```

## Good case: Building nix-ci-reproducible-false in nix-ci

```bash
nix run .\#x86_64-linux.gitlab.runnables.debug-building-nix-ci-reproducible-false-in-nix-ci
```

## Good case: Building nix-ci-reproducible-false in nix-ci-reproducible-false

```bash
nix run .\#x86_64-linux.gitlab.runnables.debug-building-nix-ci-reproducible-false-in-nix-ci-reproducible-false
```

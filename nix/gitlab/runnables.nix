{
  inputs,
  cell,
}: let
  pkgs = inputs.nixpkgs;
  inherit (inputs.cells.automation.lib) importShellScript;
  l = pkgs.lib;
  runInContainer = {
    image,
    cmd,
    ...
  }:
    pkgs.writeScriptBin "runInContainer" ''
      #!/usr/bin/env bash
      set -euo pipefail

      set -x
      if ! docker image inspect "${image.imageRefUnsafe}" >/dev/null 2>&1; then
        ${l.getExe image.copyToDockerDaemon}
      else
        echo "Image ${image.imageRefUnsafe} already exists"
      fi

      docker run --rm --tty --interactive \
        --privileged \
        --volume "$PWD:/workdir" \
        --workdir /workdir \
        --volume "$HOME/.docker:/home/user/.docker" \
        --volume "$HOME/.netrc:/home/user/.netrc" \
        --volume "$HOME/.docker:/root/.docker" \
        --volume "$HOME/.netrc:/root/.netrc" \
        "${image.imageRefUnsafe}" ${cmd} "$@"
    '';
  # Build imageExpression, collect debug info in resultDir and then run it.
  debugCmd = {
    imageExpression,
    resultDir,
  }:
    "bash -c "
    + (l.escapeShellArg ''
      set -euo pipefail;
      result_dir=${l.escapeShellArg resultDir};
      export result_dir;
      set -x;
      rm -rf $result_dir;
      mkdir -p $result_dir;
      nix build -L ${imageExpression}.copyTo;
      tag=$(nix eval --raw ${imageExpression}.imageTag);
      { set +x; } 2>/dev/null;
      for p in $(nix path-info --recursive ${imageExpression}.copyTo); do
        echo $p "$(nix hash path $p)";
      done | sort >$result_dir/paths_with_hashes.txt;
      echo "Paths with hashes in $result_dir/paths_with_hashes.txt";
      set -x
      nix copy --no-check-sigs --to $PWD/$result_dir/ ${imageExpression}.copyTo;
      { set +x; } 2>/dev/null;
      echo "Store paths in $result_dir/oci";
      echo "The actual test";
      set -x
      nix run ${imageExpression}.copyTo "oci:$result_dir/oci";
    '');
in {
  # CI container running nix as user
  nix-ci = runInContainer {
    image = cell.containers.nix-ci;
    cmd = "";
  };
  # CI container running nix as root
  nix-ci-root = runInContainer {
    image = cell.containers.nix-ci-root;
    cmd = "";
  };

  debug-building-nix-ci-in-nix-ci-root = runInContainer {
    image = cell.containers.nix-ci-root;
    cmd = debugCmd {
      imageExpression = ".#x86_64-linux.gitlab.containers.nix-ci";
      resultDir = "result_debug-building-nix-ci-in-nix-ci-root";
    };
  };

  debug-building-nix-ci-in-nix-ci = runInContainer {
    image = cell.containers.nix-ci;
    cmd = debugCmd {
      imageExpression = ".#x86_64-linux.gitlab.containers.nix-ci";
      resultDir = "result_debug-building-nix-ci-in-nix-ci";
    };
  };

  debug-building-nix-ci-reproducible-false-in-nix-ci = runInContainer {
    image = cell.containers.nix-ci;
    cmd = debugCmd {
      imageExpression = ".#x86_64-linux.gitlab.containers.nix-ci-reproducible-false";
      resultDir = "result_debug-building-nix-ci-reproducible-false-in-nix-ci";
    };
  };

  debug-building-nix-ci-reproducible-false-in-nix-ci-reproducible-false = runInContainer {
    image = cell.containers.nix-ci-reproducible-false;
    cmd = debugCmd {
      imageExpression = ".#x86_64-linux.gitlab.containers.nix-ci-reproducible-false";
      resultDir = "result_debug-building-nix-ci-reproducible-false-in-nix-ci-reproducible-false";
    };
  };

  debug-building-nix-ci-with-hello-in-nix-ci = runInContainer {
    image = cell.containers.nix-ci;
    cmd = debugCmd {
      imageExpression = ".#x86_64-linux.gitlab.containers.nix-ci-with-hello";
      resultDir = "result_debug-building-nix-ci-with-hello-in-nix-ci";
    };
  };

  debug-building-only-stdenv-in-nix-ci = runInContainer {
    image = cell.containers.nix-ci;
    cmd = debugCmd {
      imageExpression = ".#x86_64-linux.gitlab.containers.only-stdenv";
      resultDir = "result_debug-building-only-stdenv-in-nix-ci";
    };
  };

  debug-building-only-stdenv-in-nix-ci-root = runInContainer {
    image = cell.containers.nix-ci-root;
    cmd = debugCmd {
      imageExpression = ".#x86_64-linux.gitlab.containers.only-stdenv";
      resultDir = "result_debug-building-only-stdenv-in-nix-ci";
    };
  };
}

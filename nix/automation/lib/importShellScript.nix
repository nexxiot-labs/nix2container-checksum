{
  inputs,
  cell,
}: let
  defun = inputs.yants.defun;
  std = inputs.std.lib;
  nixpkgs = inputs.nixpkgs;
  l = nixpkgs.lib // builtins;
in
  /*
  Import a shell script by path.

  name: The name of the binary and outer derivation. Default: base name of the path.
  path: The path to the shell script.
  runtimeShell: The shell to use to run the script. Default: nixpkgs.runtimeShell.
  runtimeShellPrelude: Standard prelude to use for the runtime shell. Default: cell.lib.runtimeBashPrelude.
  runtimeEnv: A set of environment variables to set in the runtime shell before running the script.
  runtimeInputs: A list of derivations to add to the PATH in the runtime shell before running the script.
  */
  defun (with cell.types; [ImportShellScriptArgs inputs.yants.drv]) (
    {
      name ? builtins.baseNameOf path,
      meta ? {},
      path,
      runtimeShell ? nixpkgs.runtimeShell,
      runtimeShellPrelude ? cell.lib.runtimeBashPrelude,
      runtimeEnv ? {},
      runtimeInputs ? [],
      checkPhase ? null,
    }: let
      runtimeShell' =
        if runtimeShell != nixpkgs.runtimeShell
        then (l.getExe runtimeShell)
        else runtimeShell;
      checkScript = ''
        runHook preCheck
        ${nixpkgs.stdenv.shellDryRun} "$target"
        ${nixpkgs.shellcheck}/bin/shellcheck -x "$target"
        runHook postCheck
      '';
      checkedShellScript = std.ops.lazyDerivation {
        derivation = nixpkgs.writeTextFile {
          name = "unwrapped-${name}";
          executable = true;
          destination = "/bin/unwrapped-${name}";
          text = builtins.readFile path;
        };
      };
    in
      std.ops.lazyDerivation {
        meta =
          meta
          // {
            mainProgram = name;
          };
        derivation = nixpkgs.writeTextFile {
          inherit name;
          executable = true;
          destination = "/bin/${name}";
          text =
            ''
              #!${runtimeShell'}
              ${runtimeShellPrelude}
            ''
            + l.optionalString (runtimeInputs != []) ''
              export PATH="${l.makeBinPath runtimeInputs}"
            ''
            + l.optionalString (runtimeEnv != {}) ''
              ${l.concatStringsSep "\n" (
                l.mapAttrsToList
                (n: v: "export ${n}=${''$''}{${n}:-${l.escapeShellArg v}}")
                runtimeEnv
              )}
            ''
            + ''

              source ${l.getExe checkedShellScript} "$@"
            '';
          checkPhase = checkScript;
        };
      }
  )

{
  inputs,
  cell,
}: let
  yants = inputs.yants "nix-toolbox/nix/automation/types.nix";
  yantsImportShellScriptArgs = yants "ImportShellScriptArgs";
  yantsMeta = yants "Meta";
in rec {
  ImportShellScriptArgs = with yantsImportShellScriptArgs;
    struct "ImportShellScriptArgs" {
      name = with (yantsImportShellScriptArgs "name"); option string;
      meta = with (yantsImportShellScriptArgs "meta"); option Meta;
      path = with (yantsImportShellScriptArgs "path"); path;
      runtimeShell = with (yantsImportShellScriptArgs "runtimeShell"); option drv;
      runtimeShellPrelude = with (yantsImportShellScriptArgs "runtimeShellPrelude"); option string;
      runtimeEnv = with (yantsImportShellScriptArgs "runtimeEnv"); option (attrs string);
      runtimeInputs = with (yantsImportShellScriptArgs "runtimeInputs"); option (list drv);
      checkPhase = with (yantsImportShellScriptArgs "checkPhase"); option string;
    };
  Meta = with yantsMeta;
    struct "Meta" {
      description = with (yantsMeta "description"); option string;
    };
}

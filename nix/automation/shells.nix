{
  inputs,
  cell,
}: let
  inherit (inputs) nixpkgs namaka;
  inherit (inputs.nixpkgs.lib) mapAttrs;
  inherit (inputs.std.lib.dev) mkShell;
in
  mapAttrs (_: mkShell) {
    default = {...}: {
      name = "nix-toolbox-dev";
      commands = [
        {
          package = inputs.cells.automation.packages.std;
          category = "nix";
        }
        {
          package = nixpkgs.nix-output-monitor;
          category = "nix";
        }

        {
          package = namaka.packages.default;
          category = "nix-testing";
        }
        {
          package = nixpkgs.alejandra;
          category = "nix-testing";
        }
        {
          package = nixpkgs.nil;
          category = "nix-testing";
        }

        {
          package = nixpkgs.dive;
          category = "docker-testing";
        }
      ];
    };
  }
